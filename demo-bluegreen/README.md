# Demo: Deploying an application using blue-green deployment strategy

## Deploy the BLUE APP

Create project

```shell
oc new-project demo-bluegreen
```

Import wildfly image

```shell
oc import-image wildfly --confirm \--from quay.io/wildfly/wildfly-centos7 --insecure -n openshift
```

Create BLUE App

```shell
oc new-app wildfly~https://gitlab.com/nicknarcise/openshift-demos.git#main --context-dir=demo-bluegreen/wildfly-basic --name=blue-deploy
```

Expose Service

```shell
oc expose service blue-deploy --name=wf-bg-app
```

Get route and open in a browser

```shell
oc get route wf-bg-app
```

## Deploy the GREEN APP and switch traffic between BLUE and GREEN

Create GREEN App

```shell
oc new-app wildfly~https://gitlab.com/nicknarcise/openshift-demos.git#version2 --context-dir=demo-bluegreen/wildfly-basic --name=green-deploy
```

Patch the route to move traffic to GREEN and refresh the browser

```shell
oc patch route/wf-bg-app -p '{"spec":{"to":{"name":"green-deploy"}}}'
```

Patch the route to move traffic back to BLUE and refresh the browser

```shell
oc patch route/wf-bg-app -p '{"spec":{"to":{"name":"blue-deploy"}}}'
```

## Configure the route to weight traffic between BLUE and GREEN

Configure route

```shell
oc annotate route/wf-bg-app haproxy.router.openshift.io/balance=roundrobin
oc annotate route/wf-bg-app haproxy.router.openshift.io/disable_cookies=true
```

Update traffic split 50/50

```shell
oc set route-backends wf-bg-app blue-deploy=50 green-deploy=50
```

## Clean-up application

```shell
oc delete all --selector app=blue-deploy -o name
oc delete all --selector app=green-deploy -o name
oc delete project demo-bluegreen
```
